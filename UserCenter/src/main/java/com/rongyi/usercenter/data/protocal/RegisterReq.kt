package com.rongyi.usercenter.data.protocal

/**
 * 请求接口post参数
 *
 * @author yikwing
 * @date 2018/6/3
 */

data class RegisterReq(val phoneNum: String, val verifyCode: String, val pwd: String)