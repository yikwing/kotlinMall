package com.rongyi.usercenter.data.api

import com.rongyi.baselibrary.data.protocol.BaseResp
import com.rongyi.usercenter.data.protocal.RegisterReq
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * 请求接口
 *
 * @author yikwing
 * @date 2018/6/3
 */


interface UserApi {
    @POST("userCenter/register")
//    @FormUrlEncoded
    fun register(@Body req: RegisterReq): Observable<BaseResp<String>>
}