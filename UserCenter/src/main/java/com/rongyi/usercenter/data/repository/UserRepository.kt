package com.rongyi.usercenter.data.repository

import com.rongyi.baselibrary.data.net.RetrofitFactory
import com.rongyi.baselibrary.data.protocol.BaseResp
import com.rongyi.usercenter.data.api.UserApi
import com.rongyi.usercenter.data.protocal.RegisterReq
import io.reactivex.Observable
import javax.inject.Inject

/**
 * 构造retrofit请求参数
 *
 * @author yikwing
 * @date 2018/6/3
 */

class UserRepository @Inject constructor() {
    fun register(phoneNum: String, verifyCode: String, pwd: String): Observable<BaseResp<String>> {

        return RetrofitFactory.instance.create(UserApi::class.java)
                .register(RegisterReq(phoneNum, verifyCode, pwd))

    }

}