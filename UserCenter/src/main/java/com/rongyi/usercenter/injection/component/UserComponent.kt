package com.rongyi.usercenter.injection.component

import com.rongyi.baselibrary.injection.PerComponentScope
import com.rongyi.baselibrary.injection.component.ActivityComponent
import com.rongyi.usercenter.injection.module.UserModule
import com.rongyi.usercenter.ui.activity.RegisterActivity
import dagger.Component

/**
 * Demo class
 *
 * @author yikwing
 * @date 2018/7/9
 */

@PerComponentScope
@Component(dependencies = [(ActivityComponent::class)], modules = [(UserModule::class)])
interface UserComponent {
    fun inject(activity: RegisterActivity)
}