package com.rongyi.usercenter.injection.module

import com.rongyi.usercenter.service.UserService
import com.rongyi.usercenter.service.impl.UserServiceImpl
import dagger.Module
import dagger.Provides

/**
 * Demo class
 *
 * @author yikwing
 * @date 2018/7/9
 */


@Module
class UserModule {

    @Provides
    fun providesUserService(service: UserServiceImpl): UserService {
        return service
    }

}