package com.rongyi.usercenter.ui.activity

import android.os.Bundle
import com.rongyi.baselibrary.ui.activity.BaseMvpActivity
import com.rongyi.usercenter.R
import com.rongyi.usercenter.injection.component.DaggerUserComponent
import com.rongyi.usercenter.injection.module.UserModule
import com.rongyi.usercenter.presenter.RegisterPresenter
import com.rongyi.usercenter.presenter.view.RegisterView
import kotlinx.android.synthetic.main.activity_register.*
import org.jetbrains.anko.toast

class RegisterActivity : BaseMvpActivity<RegisterPresenter>(), RegisterView {
    override fun onRegisterResult(result: Boolean) {
        if (result) {
            toast("注册成功")
        } else {
            toast("注册失败")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        initInjection()



        mRegisterBtn.setOnClickListener {
            mPresenter.register(mPhoneNum.text.toString().trim(),
                    mVerifyCode.text.toString().trim(), "123")
        }
    }

    private fun initInjection() {
        DaggerUserComponent.builder()
                .activityComponent(activityComponent)
                .userModule(UserModule()).build().inject(this)
//        DaggerUserComponent.create().inject(this)

        mPresenter.mView = this
    }
}
