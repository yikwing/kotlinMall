package com.rongyi.usercenter.service.impl

import com.rongyi.usercenter.data.repository.UserRepository
import com.rongyi.usercenter.service.UserService
import io.reactivex.Observable
import javax.inject.Inject

/**
 * 网络请求处理
 *
 * @author yikwing
 * @date 2018/6/3
 */

class UserServiceImpl @Inject constructor() : UserService {

    @Inject
    lateinit var repository: UserRepository

    override fun register(phoneNum: String, verifyCode: String, pwd: String): Observable<Boolean> {


        return repository.register(phoneNum, verifyCode, pwd)
                .flatMap { t ->
                    if (t.status == 0) {
                        Observable.just(true)
                    } else {
                        Observable.just(false)
                    }
                }

    }
}