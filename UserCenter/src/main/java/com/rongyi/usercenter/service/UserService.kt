package com.rongyi.usercenter.service

import io.reactivex.Observable

/**
 * 网络请求
 *
 * @author yikwing
 * @date 2018/6/3
 */
interface UserService {

    fun register(phoneNum: String, verifyCode: String, pwd: String): Observable<Boolean>

}