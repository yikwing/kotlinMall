package com.rongyi.usercenter.presenter.view

import com.rongyi.baselibrary.presenter.view.BaseView

/**
 * 网络请求回调
 *
 * @author yikwing
 * @date 2018/6/3
 */
interface RegisterView : BaseView {

    fun onRegisterResult(result: Boolean)

}