package com.rongyi.usercenter.presenter

import com.rongyi.baselibrary.ext.execute
import com.rongyi.baselibrary.presenter.BasePresenter
import com.rongyi.baselibrary.rx.BaseObServer
import com.rongyi.usercenter.presenter.view.RegisterView
import com.rongyi.usercenter.service.impl.UserServiceImpl
import com.uber.autodispose.AutoDispose
import com.uber.autodispose.android.lifecycle.AndroidLifecycleScopeProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * P层
 *
 * @author yikwing
 * @date 2018/6/3
 */
class RegisterPresenter @Inject constructor() : BasePresenter<RegisterView>() {

    @Inject
    lateinit var userService: UserServiceImpl

    fun register(phoneNum: String, verifyCode: String, pwd: String) {

        userService.register(phoneNum, verifyCode, pwd)
                .execute(object : BaseObServer<Boolean>() {
                    override fun onNext(t: Boolean) {
                        mView.onRegisterResult(t)
                    }
                })

//        userService.register(phoneNum, verifyCode, pwd)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .as(AutoDispose.autoDisposable(AndroidLifecycleScopeProvider.from(this)))



    }
}