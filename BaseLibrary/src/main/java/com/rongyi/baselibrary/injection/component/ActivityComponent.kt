package com.rongyi.baselibrary.injection.component

import android.app.Activity
import com.rongyi.baselibrary.injection.ActivityScope
import com.rongyi.baselibrary.injection.module.ActivityModule
import dagger.Component

/**
 * Demo class
 *
 * @author yikwing
 * @date 2018/7/14
 */

@ActivityScope
@Component(dependencies = [(AppComponent::class)], modules = [(ActivityModule::class)])
interface ActivityComponent {
    fun activity(): Activity
}