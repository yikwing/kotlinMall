package com.rongyi.baselibrary.injection.component

import android.content.Context
import com.rongyi.baselibrary.injection.module.AppModule
import dagger.Component
import javax.inject.Singleton

/**
 * Demo class
 *
 * @author yikwing
 * @date 2018/7/14
 */

@Singleton
@Component(modules = [(AppModule::class)])
interface AppComponent {
    fun context(): Context
}