package com.rongyi.baselibrary.injection.module

import android.content.Context
import com.rongyi.baselibrary.common.BaseApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Demo class
 *
 * @author yikwing
 * @date 2018/7/14
 */

@Module
class AppModule(private val context: BaseApplication) {

    @Provides
    @Singleton
    fun providerContext(): Context {
        return context
    }

}