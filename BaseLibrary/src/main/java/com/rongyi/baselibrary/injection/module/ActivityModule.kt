package com.rongyi.baselibrary.injection.module

import android.app.Activity
import com.rongyi.baselibrary.injection.ActivityScope
import dagger.Module
import dagger.Provides

/**
 * Demo class
 *
 * @author yikwing
 * @date 2018/7/14
 */


@Module
class ActivityModule(private val activity: Activity) {

    @ActivityScope
    @Provides
    fun providerActivity(): Activity {
        return activity
    }

}