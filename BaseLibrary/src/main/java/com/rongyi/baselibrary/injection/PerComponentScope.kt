package com.rongyi.baselibrary.injection

import java.lang.annotation.Documented
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy.RUNTIME
import javax.inject.Scope

/**
 * Demo class
 *
 * @author yikwing
 * @date 2018/7/14
 */

@Scope
@Documented
@Retention(RUNTIME)
annotation class PerComponentScope {}