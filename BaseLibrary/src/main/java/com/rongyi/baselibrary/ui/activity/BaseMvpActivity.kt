package com.rongyi.baselibrary.ui.activity

import android.os.Bundle
import com.rongyi.baselibrary.common.BaseApplication
import com.rongyi.baselibrary.injection.component.ActivityComponent
import com.rongyi.baselibrary.injection.component.DaggerActivityComponent
import com.rongyi.baselibrary.injection.module.ActivityModule
import com.rongyi.baselibrary.presenter.BasePresenter
import com.rongyi.baselibrary.presenter.view.BaseView
import javax.inject.Inject

/**
 * Demo class
 *
 * @author yikwing
 * @date 2018/6/3
 */

open class BaseMvpActivity<T : BasePresenter<*>> : BaseActivity(), BaseView {
    override fun showLoading() {

    }

    override fun hideLoading() {

    }

    override fun onError() {

    }

    @Inject
    lateinit var mPresenter: T

    lateinit var activityComponent: ActivityComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initActivityInjection()
    }

    private fun initActivityInjection() {
        activityComponent = DaggerActivityComponent.builder().appComponent((application as BaseApplication).appComponent)
                .activityModule(ActivityModule(this)).build()
    }
}