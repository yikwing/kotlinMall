package com.rongyi.baselibrary.ui.activity

import android.support.v7.app.AppCompatActivity

/**
 * Demo class
 *
 * @author yikwing
 * @date 2018/6/3
 */
open class BaseActivity : AppCompatActivity() {

}