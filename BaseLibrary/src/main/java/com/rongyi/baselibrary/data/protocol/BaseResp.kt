package com.rongyi.baselibrary.data.protocol

/**
 * Demo class
 *
 * @author yikwing
 * @date 2018/6/3
 */

class BaseResp<T>(val status: Int, val message: String, val data: T)