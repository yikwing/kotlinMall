package com.rongyi.baselibrary.common

import android.app.Application
import com.rongyi.baselibrary.injection.component.AppComponent
import com.rongyi.baselibrary.injection.component.DaggerAppComponent
import com.rongyi.baselibrary.injection.module.AppModule

/**
 * Demo class
 *
 * @author yikwing
 * @date 2018/7/14
 */
class BaseApplication : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        initAppInjection()
    }

    private fun initAppInjection() {
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }

}