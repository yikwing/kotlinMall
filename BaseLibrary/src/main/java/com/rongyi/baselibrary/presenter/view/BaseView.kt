package com.rongyi.baselibrary.presenter.view

/**
 * Demo class
 *
 * @author yikwing
 * @date 2018/6/3
 */
interface BaseView {
    fun showLoading()
    fun hideLoading()
    fun onError()
}