package com.rongyi.baselibrary.presenter

import com.rongyi.baselibrary.presenter.view.BaseView

/**
 * Demo class
 *
 * @author yikwing
 * @date 2018/6/3
 */
open class BasePresenter<T : BaseView> {
    lateinit var mView: T
}