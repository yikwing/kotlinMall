package com.rongyi.baselibrary.rx

/**
 * Demo class
 *
 * @author yikwing
 * @date 2018/6/3
 */
class BaseException(val errorCode: Int, val errorMsg: String) : Throwable() {
}