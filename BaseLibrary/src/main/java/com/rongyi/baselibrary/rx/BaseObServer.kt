package com.rongyi.baselibrary.rx

import io.reactivex.Observer
import io.reactivex.disposables.Disposable

/**
 * Demo class
 *
 * @author yikwing
 * @date 2018/6/3
 */
open class BaseObServer<T> : Observer<T> {
    override fun onComplete() {
    }

    override fun onSubscribe(d: Disposable) {
    }

    override fun onNext(t: T) {
    }

    override fun onError(e: Throwable) {
    }
}