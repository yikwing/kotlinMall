package com.rongyi.kotlinmall.ui.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.rongyi.kotlinmall.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
